import React, { Component } from 'react';
import './TodoForm.css';

class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const target = e.target;
        const name = target.name;
        
        this.setState({
            [name]: target.value,
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.title !== '' && this.state.content !== '') {
            this.props.onSubmit(this.state);
            this.setState({
                title: '',
                content: '',
            });
        }
    }

    render() {
        return (
            <div className="TodoForm">
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <span>Title</span>
                        <input type="text" name="title" value={this.state.title} onChange={this.handleChange}/>
                    </div>
                    <div>
                        <span>Content</span>
                        <textarea type="text" name="content" value={this.state.content} onChange={this.handleChange}/>
                    </div>
                    <button type="submit" name="submit">Add</button>
                </form>
            </div>
        );
    }
}

export default TodoForm;
