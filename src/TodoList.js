import React, { Component } from 'react';
import Todo from './Todo';
import './TodoList.css';

class TodoList extends Component {
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(id) {
        this.props.handleDelete(id);
    }

    render() {
        return (
            <div className="TodoList">
                {this.props.todos.map((todo, key) => (
                    <Todo {...todo} id={key} key={key} onDelete={this.handleDelete}/>
                ))}
            </div>
        );
    }
}

export default TodoList;
