import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
    };
    this.handleTodoCreation = this.handleTodoCreation.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleTodoCreation(todo) {
    this.setState((prevState) => ({
      todos: prevState.todos.concat(todo),
    }));
  }

  handleDelete(id) {
    this.setState((prevState) => ({
      todos: prevState.todos.filter((_, index) => index !== id),
    }));
  }

  render() {
    return (
      <div>
        <TodoForm onSubmit={this.handleTodoCreation}/>
        <TodoList todos={this.state.todos} handleDelete={this.handleDelete}/>
      </div>
    );
  }
}

export default App;
