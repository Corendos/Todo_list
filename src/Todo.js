import React, { Component } from 'react';
import './Todo.css';

class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            done: false,
        };
        this.handleCheckbox = this.handleCheckbox.bind(this);
    }
    
    handleCheckbox(e) {
        this.setState({
            done: e.target.checked,
        })
    };

    render() {
        return (
            <div className="Todo">
                <input type="checkbox" checked={this.state.done} onChange={this.handleCheckbox}/>
                <span><strong>{this.props.title}</strong> - {this.props.content}</span>
                <button onClick={() => this.props.onDelete(this.props.id)}>x</button>
            </div>
        );
    }
}

export default Todo;
